﻿
// MFCApplication1Doc.h: интерфейс класса CMFCApplication1Doc 
//


#pragma once

#include "Graph.h"
#include "Figure.h"
#include <unordered_set>
#include <tuple>

#define MIN_RECTANGLE_SIZE 100
#define MAX_RECTANGLE_SIZE 1024
#define MAX_CIRCLES_COUNT 500

struct CSolve
{
	std::shared_ptr<CEdge<CFigure>> Edge;
	std::list<std::shared_ptr<CVertex<CFigure>>> AdditionalVertexes;
	float Distance;
	CPoint Position;
};

class CMFCApplication1Doc : public CDocument
{
protected: 
	CMFCApplication1Doc() noexcept;
	DECLARE_DYNCREATE(CMFCApplication1Doc)

public:
	virtual BOOL OnNewDocument() override;

	void DrawRectangle(CDC* pDC);
	void DrawCircles(CDC* pDC);
	void DrawFreeCirclesText(CDC* pDC);
	CSize GetRectangleSize() { return CSize(m_rectangle->GetWidth(), m_rectangle->GetHeight()); }

	int m_nTextLength = 0;

public:
	virtual ~CMFCApplication1Doc();

	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName) override;

protected:
	DECLARE_MESSAGE_MAP()

private:
	void makeRectangle();
	void makeSircles();

	void packing();

	void checkEdges(const std::shared_ptr<CVertex<CFigure>>& vertex, CCircle& circle);

	bool IsPositionInRectangle(const CPoint& position) const;

	std::list<CPoint> calculateCirclePosition(const std::shared_ptr<CVertex<CFigure>>& vertex, const std::shared_ptr<CVertex<CFigure>>& pairVertex, CCircle& circle);

	std::list<CPoint> calculateThirdSirclePoint(const CCircle& A, const CCircle& B, CCircle& C);
	std::list<CPoint> calculateBorderPoint(const CLine& Line, const CCircle& CirclePlaced, CCircle& Circle);
	std::list<CPoint> calculateAnglePoint(const CLine& Line1, const CLine& Line2, CCircle& Circle);
	float calculateMinDistance(const std::shared_ptr<CEdge<CFigure>> edge, const CPoint& position, const int nRadius);
	float calculateDistance(const std::shared_ptr<CFigure> fig, const CPoint& position, const int nRadius);
	void fillGraphByRectangle();

private:
	CGraph<CFigure> m_graph;
	std::unique_ptr<CRectangle> m_rectangle = nullptr;
	std::list<CCircle> m_circles;
	CSolve m_bestResult;
	std::unordered_set<std::shared_ptr<CVertex<CFigure>>> m_checkedVertexes;
};
