#include "pch.h"
#include "Figure.h"

CRectangle::CRectangle(int width, int height)
	: m_nWidth(width), m_nHeight(height)
{
	m_figureType = eFigureType::Rectangle;

	m_lines.emplace_back(std::make_shared<CLine>(CLine(eLineType::Horisontal, CPoint(0, 0), CPoint(m_nWidth, 0))));
	m_lines.emplace_back(std::make_shared<CLine>(CLine(eLineType::Vertical, CPoint(m_nWidth, 0), CPoint(m_nWidth, m_nHeight))));
	m_lines.emplace_back(std::make_shared<CLine>(CLine(eLineType::Horisontal, CPoint(m_nWidth, m_nHeight), CPoint(0, m_nHeight))));
	m_lines.emplace_back(std::make_shared<CLine>(CLine(eLineType::Vertical, CPoint(0, m_nHeight), CPoint(0, 0))));
}

void CRectangle::Draw(CDC* pDC)
{
	for(auto &line : m_lines)
		line->Draw(pDC);
}

CCircle::CCircle(int radius)
	: m_nRadius(radius)
{
	m_figureType = eFigureType::Circle;

	m_nWeight = m_nRadius * m_nRadius;
}

void CCircle::Draw(CDC* pDC)
{
	CRect rect;
	rect.top = m_DrawPosition.y - m_nRadius;
	rect.left = m_DrawPosition.x - m_nRadius;
	rect.bottom = m_DrawPosition.y + m_nRadius;
	rect.right = m_DrawPosition.x + m_nRadius;

	pDC->Ellipse(rect);
}

void CCircle::SetDrawPosition(CPoint position)
{
	m_DrawPosition = position;
}

CLine::CLine(eLineType type, CPoint start, CPoint end)
	: m_lineType(type), m_start(start), m_end(end)
{
	m_start.Offset(DRAWING_OFFSET, DRAWING_OFFSET);
	m_end.Offset(DRAWING_OFFSET, DRAWING_OFFSET);
	m_figureType = eFigureType::Line;
}

void CLine::Draw(CDC* pDC)
{
	pDC->MoveTo(m_start);
	pDC->LineTo(m_end);
}

int CLine::GetLength() const
{
	int nLength = 0;

	if (m_lineType == eLineType::Horisontal)
		nLength = max(m_end.x, m_start.x) - min(m_end.x, m_start.x);
	else
		nLength = max(m_end.y, m_start.y) - min(m_end.y, m_start.y);

	return nLength;
}
