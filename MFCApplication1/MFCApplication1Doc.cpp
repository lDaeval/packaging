﻿
// MFCApplication1Doc.cpp: реализация класса CMFCApplication1Doc 
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS можно определить в обработчиках фильтров просмотра реализации проекта ATL, эскизов
// и поиска; позволяет совместно использовать код документа в данным проекте.
#ifndef SHARED_HANDLERS
#include "MFCApplication1.h"
#endif

#define _USE_MATH_DEFINES

#include "MFCApplication1Doc.h"

#include <stdlib.h>
#include <time.h>
#include <propkey.h>
#include <math.h>
#include <algorithm>
#include <limits>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RECTANGLE_KEYWORD "Rectangle"
#define START_RECTANGLE_BLOCK "["
#define END_RECTANGLE_BLOCK "]"

#define CIRCLES_KEYWORD "Circles"
#define START_CIRCLE_BLOCK "{"
#define END_CIRCLE_BLOCK "}"
#define SEPARATOR ","

// CMFCApplication1Doc

IMPLEMENT_DYNCREATE(CMFCApplication1Doc, CDocument)

BEGIN_MESSAGE_MAP(CMFCApplication1Doc, CDocument)
END_MESSAGE_MAP()


// Создание или уничтожение CMFCApplication1Doc

CMFCApplication1Doc::CMFCApplication1Doc() noexcept
{


}

CMFCApplication1Doc::~CMFCApplication1Doc()
{
}

BOOL CMFCApplication1Doc::OnOpenDocument(LPCTSTR lpszPathName)
{
	m_circles.clear();

	CFile file(lpszPathName, CFile::modeRead);

	CString fileData;
	UINT nBytes = (UINT)file.GetLength();
	int nChars = nBytes / sizeof(TCHAR);
	nBytes = file.Read(fileData.GetBuffer(nChars), nBytes);
	fileData.ReleaseBuffer(nChars);

	int nWidth = 0;
	int nHeight = 0;

	int nPos = 0;
	int nValuePos;
	CString sValue;
	nPos = fileData.Find(RECTANGLE_KEYWORD, nPos) + _countof(RECTANGLE_KEYWORD);
	nValuePos = nPos = fileData.Find(START_RECTANGLE_BLOCK, nPos) + 1;
	nPos = fileData.Find(SEPARATOR, nPos);

	sValue = fileData.Mid(nValuePos, nPos - nValuePos);
	sValue.Trim();
	nWidth = atoi(sValue);

	nValuePos = nPos += 1;
	nPos = fileData.Find(END_RECTANGLE_BLOCK, nPos);

	sValue = fileData.Mid(nValuePos, nPos - nValuePos);
	sValue.Trim();
	nHeight = atoi(sValue);

	m_rectangle = std::make_unique<CRectangle>(nWidth, nHeight);

	int nRadius = 0;

	nPos = fileData.Find(CIRCLES_KEYWORD, nPos) + _countof(CIRCLES_KEYWORD);
	nValuePos = nPos = fileData.Find(START_CIRCLE_BLOCK, nPos);
	int nLastPos = fileData.Find(END_CIRCLE_BLOCK, nPos);

	while (nPos != -1)
	{
		nPos += 1;
		nValuePos = nPos;

		nPos = fileData.Find(SEPARATOR, nPos);

		if(nPos == -1)
			sValue = fileData.Mid(nValuePos, nLastPos - nValuePos);
		else
			sValue = fileData.Mid(nValuePos, nPos - nValuePos);
		sValue.Trim();

		nRadius = atoi(sValue);

		m_circles.push_back(nRadius);
	}

	m_circles.sort([](const CCircle& a, const CCircle& b)
		{
			return a.GetRadius() > b.GetRadius();
		});

	packing();

	return FALSE;
}

void CMFCApplication1Doc::makeRectangle()
{
	srand((unsigned int)time(0));
	int nHeight = MIN_RECTANGLE_SIZE + (rand() % (MAX_RECTANGLE_SIZE + 1 - MIN_RECTANGLE_SIZE));
	int nWidth = MIN_RECTANGLE_SIZE + (rand() % (MAX_RECTANGLE_SIZE + 1 - MIN_RECTANGLE_SIZE));

	m_rectangle = std::make_unique<CRectangle>(nWidth, nHeight);
}

void CMFCApplication1Doc::makeSircles()
{
	m_circles.clear();

	srand((unsigned int)time(0));

	for (int i = 0; i < MAX_CIRCLES_COUNT; i++)
	{
		m_circles.push_back((rand() % (max(m_rectangle->GetHeight(), m_rectangle->GetWidth()) + 1)));
	}

	m_circles.sort([](const CCircle& a, const CCircle& b)
		{
			return a.GetRadius() > b.GetRadius();
		});
}

void CMFCApplication1Doc::packing()
{
	fillGraphByRectangle();
	for (auto& circle : m_circles)
	{
		m_bestResult.Edge = nullptr;
		m_bestResult.Distance = FLT_MAX;

		m_checkedVertexes.clear();

		if(circle.GetRadius() < m_rectangle->GetHeight() && circle.GetRadius() < m_rectangle->GetWidth())
			checkEdges(m_graph.GetHead(), circle);

		if (!m_bestResult.Edge)
		{
			circle.SetDrawPosition(CPoint(0, 0));
			continue;
		}

		circle.SetDrawPosition(m_bestResult.Position);
		
		auto newVertex = std::make_shared<CVertex<CFigure>>(std::make_shared<CCircle>(circle));
		m_graph.CreateEdge(m_bestResult.Edge->GetVertexes().first.lock(), newVertex);
		m_graph.CreateEdge(m_bestResult.Edge->GetVertexes().second.lock(), newVertex);

		for (auto& additional : m_bestResult.AdditionalVertexes)
		{
			m_graph.CreateEdge(m_bestResult.Edge->GetVertexes().first.lock(), additional);
			m_graph.CreateEdge(m_bestResult.Edge->GetVertexes().second.lock(), additional);
		}
	}
}

void CMFCApplication1Doc::checkEdges(const std::shared_ptr<CVertex<CFigure>>& vertex, CCircle& circle)
{
	if (m_checkedVertexes.find(vertex) != m_checkedVertexes.end())
		return;

	for (auto& edge : vertex->GetEdges())
	{
		//auto pairVertex = edge->GetPairVertex(vertex);

		for (auto& pairVertex : m_graph.GetVertexes())
		{
			if (pairVertex == vertex || m_checkedVertexes.find(vertex) != m_checkedVertexes.end())
				continue;

			std::list<CPoint> positions = calculateCirclePosition(vertex, pairVertex, circle);

			for (auto& position : positions)
			{
				if (!IsPositionInRectangle(position))
					continue;

				CEdge<CFigure> newEdge(vertex, pairVertex);
				float fDistance = calculateMinDistance(std::make_shared<CEdge<CFigure>>(newEdge)/*edge*/, position, circle.GetRadius());

				if (fDistance < m_bestResult.Distance && fDistance >= 0)
				{
					m_bestResult.Edge = std::make_shared<CEdge<CFigure>>(newEdge)/*edge*/;
					m_bestResult.Distance = fDistance;
					m_bestResult.Position = position;
				}
			}
		}

		m_checkedVertexes.insert(vertex);

		checkEdges(edge->GetPairVertex(vertex), circle);
	}
}

bool CMFCApplication1Doc::IsPositionInRectangle(const CPoint &position) const
{
	bool bInLeftSide = position.x > DRAWING_OFFSET;
	bool bInRightSide = position.x < (m_rectangle->GetWidth() + DRAWING_OFFSET);
	bool bInTopSide = position.y > DRAWING_OFFSET;
	bool bInBottomSide = position.y < (m_rectangle->GetHeight() + DRAWING_OFFSET);

	return bInLeftSide && bInRightSide && bInTopSide && bInBottomSide;
}

std::list<CPoint> CMFCApplication1Doc::calculateCirclePosition(const std::shared_ptr<CVertex<CFigure>>& vertex, const std::shared_ptr<CVertex<CFigure>>& pairVertex, CCircle& circle)
{
	auto first = vertex->GetValue();
	auto second = pairVertex->GetValue();

	int combine = (int)first->GetFigureType() + (int)second->GetFigureType();
	switch (combine)
	{
	case (int)eFigureType::Line + (int)eFigureType::Line:
	{
		std::shared_ptr<CLine> line1 = std::dynamic_pointer_cast<CLine>(first);
		std::shared_ptr<CLine> line2 = std::dynamic_pointer_cast<CLine>(second);

		return calculateAnglePoint(*line1, *line2, circle);
	}
	break;
	case (int)eFigureType::Line + (int)eFigureType::Circle:
	{
		std::shared_ptr<CLine> line = std::dynamic_pointer_cast<CLine>(first->GetFigureType() == eFigureType::Line ? first : second);
		std::shared_ptr<CCircle> circlePlaced = std::dynamic_pointer_cast<CCircle>(first->GetFigureType() == eFigureType::Circle ? first : second);

		return calculateBorderPoint(*line, *circlePlaced, circle);
	}
	break;
	case (int)eFigureType::Circle + (int)eFigureType::Circle:
	{
		auto circle1 = std::dynamic_pointer_cast<CCircle>(first);
		auto circle2 = std::dynamic_pointer_cast<CCircle>(second);

		return calculateThirdSirclePoint(*circle1, *circle2, circle);
	}
	break;
	default:
		break;
	}

	return std::list<CPoint>();
}

std::list<CPoint> CMFCApplication1Doc::calculateThirdSirclePoint(const CCircle& A, const CCircle& B, CCircle& C)
{
	const CPoint& APos = A.GetPosition();
	const CPoint& BPos = B.GetPosition();

	int nARad = A.GetRadius();
	int nBRad = B.GetRadius();
	int nCRad = C.GetRadius();

	int nASide = nBRad + nCRad;
	int nBSide = nARad + nCRad;
	int nCSide = nARad + nBRad;

	std::list<CPoint> positions;

	if ((nASide + nBSide) <= nCSide && (nASide + nCSide) <= nBSide && (nBSide + nCSide) <= nASide)
		return positions;

	float fCosA = (pow((float)nBSide, 2) + pow((float)nCSide, 2) - pow((float)nASide, 2)) / (2 * (float)nBSide * nCSide);
	float fA = acos(fCosA);

	float fKAngle = atan2((BPos.y - APos.y), (BPos.x - APos.x));
	float fAngle = fKAngle + fA;

	int nX = APos.x + (nBSide * cos(fAngle));
	int nY = APos.y + (nBSide * sin(fAngle));

	positions.push_back(CPoint(nX, nY));

	fAngle = fKAngle - fA;
	nX = APos.x + (nBSide * cos(fAngle));
	nY = APos.y + (nBSide * sin(fAngle));

	positions.push_back(CPoint(nX, nY));

	return positions;
}

std::list<CPoint> CMFCApplication1Doc::calculateBorderPoint(const CLine& Line, const CCircle& CirclePlaced, CCircle& Circle)
{
	std::list<CPoint> positions;
	CPoint position;

	if (Line.GetType() == eLineType::Horisontal)
	{

		position.y = Line.GetStartPoint().y + Circle.GetRadius();
		position.x = sqrt(pow((CirclePlaced.GetRadius() + Circle.GetRadius()), 2) - pow(CirclePlaced.GetPosition().y, 2) +
			(2 * CirclePlaced.GetPosition().y * position.y) - pow(position.y, 2)) + CirclePlaced.GetPosition().x;

		positions.push_back(position);

		position.y = Line.GetStartPoint().y - Circle.GetRadius();
		position.x = sqrt(pow((CirclePlaced.GetRadius() + Circle.GetRadius()), 2) - pow(CirclePlaced.GetPosition().y, 2) +
			(2 * CirclePlaced.GetPosition().y * position.y) - pow(position.y, 2)) + CirclePlaced.GetPosition().x;

		positions.push_back(position);
	}
	else
	{
		position.x = Line.GetStartPoint().x + Circle.GetRadius();
		position.y = sqrt(pow((CirclePlaced.GetRadius() + Circle.GetRadius()), 2) - pow(CirclePlaced.GetPosition().x, 2) + 
			(2 * CirclePlaced.GetPosition().x * position.x) - pow(position.x, 2)) + CirclePlaced.GetPosition().y;

		positions.push_back(position);

		position.x = Line.GetStartPoint().x - Circle.GetRadius();
		position.y = sqrt(pow((CirclePlaced.GetRadius() + Circle.GetRadius()), 2) - pow(CirclePlaced.GetPosition().x, 2) +
			(2 * CirclePlaced.GetPosition().x * position.x) - pow(position.x, 2)) + CirclePlaced.GetPosition().y;

		positions.push_back(position);
	}

	return positions;
}


std::list<CPoint> CMFCApplication1Doc::calculateAnglePoint(const CLine& Line1, const CLine& Line2, CCircle& Circle)
{
	const CLine& Horisontal = Line1.GetType() == eLineType::Horisontal ? Line1 : Line2;
	const CLine& Vertical = Line2.GetType() == eLineType::Vertical ? Line2 : Line1;

	if (Horisontal.GetType() == Vertical.GetType())
		return std::list<CPoint>();

	int nXDirection = 1;
	int nYDirection = 1;
	CPoint anglePoint;

	if (Horisontal.GetEndPoint() == Vertical.GetStartPoint())
	{
		anglePoint = Horisontal.GetEndPoint();

		nXDirection = Horisontal.GetStartPoint().x - anglePoint.x;
		nXDirection = nXDirection / abs(nXDirection);

		nYDirection = Vertical.GetEndPoint().y - anglePoint.y;
		nYDirection = nYDirection / abs(nYDirection);

	}
	else
	{
		anglePoint = Horisontal.GetStartPoint();

		nXDirection = Horisontal.GetEndPoint().x - anglePoint.x;
		nXDirection = nXDirection / abs(nXDirection);

		nYDirection = Vertical.GetStartPoint().y - anglePoint.y;
		nYDirection = nYDirection / abs(nYDirection);
	}

	int nX = anglePoint.x + (Circle.GetRadius() * nXDirection);
	int nY = anglePoint.y + (Circle.GetRadius() * nYDirection);

	std::list<CPoint> positions{ CPoint(nX, nY) };

	return positions;
}

float CMFCApplication1Doc::calculateMinDistance(const std::shared_ptr<CEdge<CFigure>> edge, const CPoint& position, const int nRadius)
{
	float fDistance = FLT_MAX;

	for (auto& vertex : m_graph.GetVertexes())
	{
		if(edge->GetPairVertex(vertex))
			continue;

		fDistance = min(fDistance, calculateDistance(vertex->GetValue(), position, nRadius));

		if (fDistance < 0)
			break;

		if (fDistance < m_bestResult.Distance)
			m_bestResult.AdditionalVertexes.clear();

		if (fDistance <= std::numeric_limits<float>::epsilon())
			m_bestResult.AdditionalVertexes.push_back(vertex);
	}

	return fDistance;
}

float CMFCApplication1Doc::calculateDistance(const std::shared_ptr<CFigure> fig, const CPoint& position, const int nRadius)
{
	float fDistance = 0;
	switch (fig->GetFigureType())
	{
	case eFigureType::Line:
	{
		auto line = std::dynamic_pointer_cast<CLine>(fig);

		if (line->GetType() == eLineType::Horisontal)
			fDistance = std::abs(line->GetStartPoint().y - position.y) - nRadius;
		else
			fDistance = std::abs(line->GetStartPoint().x - position.x) - nRadius;
	}
	break;
	case eFigureType::Circle:
	{
		auto figure = std::dynamic_pointer_cast<CCircle>(fig);
		CPoint pos = figure->GetPosition();

		fDistance = sqrt(pow(((float)pos.x - (float)position.x), 2) + pow(((float)pos.y - (float)position.y), 2))
			- figure->GetRadius() - nRadius;
	}
	break;
	default:
		break;
	}

	return fDistance;
}

void CMFCApplication1Doc::fillGraphByRectangle()
{
	m_graph.Clear();
	auto vertex = m_graph.GetHead();
	for (auto& line : m_rectangle->GetLines())
	{
		if (!vertex)
		{
			m_graph.CreateHead(line);
			vertex = m_graph.GetHead();
		}
		else
		{
			auto newVertex = std::make_shared<CVertex<CFigure>>(line);
			m_graph.CreateEdge(vertex, newVertex);
			vertex = newVertex;
		}
	}

	m_graph.CreateEdge(vertex, m_graph.GetHead());
}

BOOL CMFCApplication1Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	makeRectangle();
	makeSircles();
	
	packing();

	return TRUE;
}

void CMFCApplication1Doc::DrawRectangle(CDC* pDC)
{
	if (m_rectangle == nullptr)
		return;

	m_rectangle->Draw(pDC);
}

void CMFCApplication1Doc::DrawCircles(CDC* pDC)
{
	for (auto& circle : m_circles)
	{
		if (circle.GetPosition().x != 0 && circle.GetPosition().y != 0)
			circle.Draw(pDC);
	}
}

void CMFCApplication1Doc::DrawFreeCirclesText(CDC* pDC)
{
	CString sText;
	for (auto& circle : m_circles)
	{
		if (circle.GetPosition().x == 0 && circle.GetPosition().y == 0)
		{
			sText.AppendFormat("%d, ", circle.GetRadius());
		}
	}

	sText.Delete(sText.GetLength() - 2, 2);

	m_nTextLength = sText.GetLength()*7;

	pDC->TextOutA(DRAWING_OFFSET, m_rectangle->GetHeight() + DRAWING_OFFSET * 2, sText);
}
