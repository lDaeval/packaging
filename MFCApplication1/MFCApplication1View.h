﻿
// MFCApplication1View.h: интерфейс класса CMFCApplication1View
//

#pragma once


class CMFCApplication1View : public CScrollView
{
protected: // создать только из сериализации
	CMFCApplication1View() noexcept;
	DECLARE_DYNCREATE(CMFCApplication1View)

// Атрибуты
public:
	CMFCApplication1Doc* GetDocument() const;
	void OnInitialUpdate() override;
// Операции
public:

// Переопределение
public:
	virtual void OnDraw(CDC* pDC);  // переопределено для отрисовки этого представления

// Реализация
public:
	virtual ~CMFCApplication1View();

protected:

// Созданные функции схемы сообщений
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};

inline CMFCApplication1Doc* CMFCApplication1View::GetDocument() const
   { return reinterpret_cast<CMFCApplication1Doc*>(m_pDocument); }
