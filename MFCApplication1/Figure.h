#pragma once
#include <afxwin.h>
#include <list>
#include <memory>

#define DRAWING_OFFSET 10

enum class eLineType { Horisontal, Vertical };
enum class eFigureType { None, Line, Rectangle, Circle };

class CFigure
{
public:
	eFigureType GetFigureType() { return m_figureType; }

protected:
	virtual void Draw(CDC* pDC) {};

protected:
	eFigureType m_figureType = eFigureType::None;
};

class CLine : public CFigure
{
public:
	CLine(eLineType type, CPoint start, CPoint end);

	void Draw(CDC* pDC) override;
	int GetLength() const;
	eLineType GetType() const { return m_lineType; };
	const CPoint& GetStartPoint() const { return m_start; }
	const CPoint& GetEndPoint() const { return m_end; }

private:
	CPoint m_start; 
	CPoint m_end;
	eLineType m_lineType;
};

class CRectangle : public CFigure
{
public:
	CRectangle(int width, int height);

	void Draw(CDC* pDC) override;

	int GetWidth() const { return m_nWidth; }
	int GetHeight() const { return m_nHeight; }

	const std::list<std::shared_ptr<CLine>>& GetLines() const { return m_lines; }

private:
	int m_nWidth = 0;
	int m_nHeight = 0;

	std::list<std::shared_ptr<CLine>> m_lines;
};

class CCircle : public CFigure
{
public:
	CCircle(int radius);

	void Draw(CDC* pDC) override;

	int GetWeight() const { return m_nWeight; };
	void SetDrawPosition(CPoint position);
	const CPoint& GetPosition() const { return m_DrawPosition; }
	int GetRadius() const { return m_nRadius; }

private:
	int m_nRadius = 0;
	int m_nWeight = 0;
	CPoint m_DrawPosition = CPoint(0, 0);
};