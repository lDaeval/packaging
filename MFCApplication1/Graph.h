#pragma once
#include <list>
#include <utility>
#include <memory>
#include <set>

template<class T> class CVertex;
template<class T> class CEdge;

template<class T>
class CGraph
{
public:
	std::shared_ptr<CVertex<T>> GetHead() { return m_head; }

	void CreateHead(std::shared_ptr<T> value)
	{
		m_head = std::make_shared<CVertex<T>>(value);
	}

	void CreateEdge(std::shared_ptr<CVertex<T>>& vertex1, std::shared_ptr<CVertex<T>>& vertex2)
	{
		auto edge = std::make_shared<CEdge<T>>(CEdge<T>(vertex1, vertex2));

		vertex1->AddEdge(edge);
		vertex2->AddEdge(edge);

		m_vertexes.insert(vertex1);
		m_vertexes.insert(vertex2);
	}

	const std::set<std::shared_ptr<CVertex<T>>>& GetVertexes() { return m_vertexes; }

	void Clear()
	{
		m_head.reset();
		m_vertexes.clear();
	}

private:
	std::shared_ptr<CVertex<T>> m_head = nullptr;
	std::set<std::shared_ptr<CVertex<T>>> m_vertexes;
};

template<class T>
class CEdge
{
public:
	CEdge(std::shared_ptr<CVertex<T>> vertex1, std::shared_ptr<CVertex<T>> vertex2) :
		m_vertexes(vertex1, vertex2) 
	{
	};

	std::shared_ptr<CVertex<T>> GetPairVertex(std::shared_ptr<CVertex<T>> vertex) const
	{
		std::shared_ptr<CVertex<T>> result = nullptr;

		if (m_vertexes.first.lock() == vertex)
			result = m_vertexes.second.lock();
		else if(m_vertexes.second.lock() == vertex)
			result = m_vertexes.first.lock();

		return result;
	}

	const std::pair<std::weak_ptr<CVertex<T>>, std::weak_ptr<CVertex<T>>> GetVertexes() const { return m_vertexes; }

private:
	std::pair<std::weak_ptr<CVertex<T>>, std::weak_ptr<CVertex<T>>> m_vertexes;
};

template<class T>
class CVertex
{
public:
	CVertex(std::shared_ptr<T> value)
	{
		m_value = value;
	}

	~CVertex()
	{
		m_edges.clear();
	}

	size_t GetEdgesCount() const { return m_edges.size(); }

	const std::list<std::shared_ptr<CEdge<T>>>& GetEdges() const
	{
		return m_edges;
	}

	void AddEdge(std::shared_ptr<CEdge<T>> edge)
	{
		m_edges.push_back(edge);
	}

	std::shared_ptr<T> const GetValue() const { return m_value; }

protected:
	std::list<std::shared_ptr<CEdge<T>>> m_edges;
	std::shared_ptr<T> m_value = nullptr;

};
