﻿
// MFCApplication1View.cpp: реализация класса CMFCApplication1View
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS можно определить в обработчиках фильтров просмотра реализации проекта ATL, эскизов
// и поиска; позволяет совместно использовать код документа в данным проекте.
#ifndef SHARED_HANDLERS
#include "MFCApplication1.h"
#endif

#include "MFCApplication1Doc.h"
#include "MFCApplication1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCApplication1View

IMPLEMENT_DYNCREATE(CMFCApplication1View, CScrollView)

BEGIN_MESSAGE_MAP(CMFCApplication1View, CScrollView)
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// Создание или уничтожение CMFCApplication1View

CMFCApplication1View::CMFCApplication1View() noexcept
{
	// TODO: добавьте код создания

}

CMFCApplication1View::~CMFCApplication1View()
{
}

void CMFCApplication1View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_F5)
	{
		CMFCApplication1Doc* pDoc = GetDocument();
		pDoc->OnNewDocument();
		Invalidate();
		UpdateWindow();
	}
}

void CMFCApplication1View::OnDraw(CDC* pDC)
{
	CMFCApplication1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	pDoc->DrawRectangle(pDC);
	pDoc->DrawCircles(pDC);
	pDoc->DrawFreeCirclesText(pDC);

	CSize sizeTotal;
	sizeTotal.cx = max(pDoc->GetRectangleSize().cx, pDoc->m_nTextLength) + (DRAWING_OFFSET * 2);
	sizeTotal.cy = pDoc->GetRectangleSize().cy + (DRAWING_OFFSET * 10);
	SetScrollSizes(MM_TEXT, sizeTotal);

	// TODO: добавьте здесь код отрисовки для собственных данных
}

void CMFCApplication1View::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CMFCApplication1Doc* pDoc = GetDocument();

	CSize sizeTotal;
	sizeTotal.cx = pDoc->GetRectangleSize().cx + (DRAWING_OFFSET * 2);
	sizeTotal.cy = pDoc->GetRectangleSize().cy + (DRAWING_OFFSET * 10);
	SetScrollSizes(MM_TEXT, sizeTotal);
}
